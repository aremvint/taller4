taller4: taller4.o cifrado.o
	gcc -g obj/taller4.o obj/cifrado.o -o bin/taller4

taller4.o: src/main.c
	gcc -g -Wall -c -I include/ src/main.c -o obj/taller4.o

cifrado.o: src/cifrado.c include/cifrado.h
	gcc -g -Wall -c -I include/ src/cifrado.c -o obj/cifrado.o
